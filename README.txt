Hello all,

I have been wanting to do that for a while and didn't really know how
to organise it, but I think I finally found something that will be
"easy" for the developers in this group.

Right now, everyone is doing its scripts alone in its corner, but
there are sometimes patches, ideas and improvements that could be
shared between everyone and we could start share development in a
better way that just bits of codes in emails and flickr topics.

For this, I have set up a version control repository where scripts (or
whatever else is developed/discussed here) can be developed. There is
however a requirement of the host: - we don't go over 100Mb (which is
quite big) - the content is free software

The public repository is here: repo.or.cz/w/FlickrHacks.git Anyone can
access and clone it using git or the web interface. If you want to
"push" commits to the public repository, contact me and I'll add you
to the developer list, or just send me formatted patches by email.

Currently, there is a quite flat structure: 

_greasemonkey_/ 
   .... (all the user scripts are here).
_greasemonkey_tools_/
   .... (toolkits to help develop greasemonkey scripts for flickr)

there could be other categories of development, we'll see.

See the discussion topic:
http://flickr.com/groups/flickrhacks/discuss/72157601429332744/
